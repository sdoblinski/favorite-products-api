# FAVORITE PRODUCTS API #

## Setup
Com o `docker-compose` instalado:
```
docker-compose up --build
```
Para rodar os testes:
```
cd tests
npm install
npm run test
```

## Endpoints
* `POST /login` autentica admin. Retorna accessToken a ser usado no header `Authorization` como Bearer em todos os próximos endpoints.
Payload de login:
```
{
	"user": "admin",
	"password": "admin"
}
```
* `POST /client` cria cliente. Retorna cliente criado.
Exemplo de payload:
```
{
	"email": "simeidoblinski@gmail.com",
	"name": "Simei Doblinski"
}
```
* `GET /client/:clientId` lê cliente. Retorna cliente criado.
* `POST /client/:clientId` atualiza cliente. Retorna cliente atualizado.
Exemplo de payload:
```
{
	"email": "simei.novo.email@gmail.com",
	"name": "Simei Doblinski Novo"
}
```
* `DELETE /client/:clientId` remove cliente. Retorna mensagem de confirmação.
* `PUT /client/:clientId/favorite-product/:productId` adiciona produto à lista de favoritos do cliente. Por ser um endpoint que coloca uma ação na fila, apenas retorna recebimento da requisição.
* `DELETE /client/:clientId/favorite-product/:productId` remove produto da lista de favoritos do cliente. Por ser um endpoint que coloca uma ação na fila, apenas retorna recebimento da requisição.

### Observação
As tecnologias Express.js como Gateway e Redis Pub Sub como mensageria foram usadas para ilustrar a proposta. Há ferramentas mais adequeadas para rodar em produção como Express Gateway, Nginx, AWS API Gateway, Kafka, RabbitMQ, AWS SQS, entre muitos outros.

### Observação 2
Originalmente a api usava o endpoint de detalhe de produto informado na documentação do desafio técnico (`http://challenge-api.luizalabs.com/api/product/<ID>`), mas esse endpoint passou a retornar 404 para todos IDs de produtos, por isso a API está usando, provisoriamente,	 uma lista propria de produtos localizada em `./favorite-product-persister/src/product/products.js`, esses são os produtos considerados existentes para a API.


## Arquitetura
A proposta dessa API considera fundamentais a escalabilidade, performance e alta disponibilidade. É baseada em microsserviços, mensageria e cache.
Pora razões de simplicidade de implementação e tempo alguns aspectos foram simplificados, os casos serão mencionados :)

## Tecnologias usadas
* Docker/Docker compose: containers
* Express.js: serviços HTTP
* MongoDB e Redis: bancos de dados
* Redis: mensageria
* Jest: testes
* Eslint standard

## Organização do código
Numa arquitetura de microsserviços corre-se o risco de repetir ou descentralizar código reutilizado entre vários recursos. Módulos e pacotes NPM foram usados pensando no encapsulamento de responsabilidades e reutilização do código. É simulado um repositório próprio na pasta `common` com os pacotes databases, entidades client e favorite product, error handler e oauth. Por simplicidade de implementação esses pacotes são instalados localmente a partir de uma cópia feita no build do docker. Idealmente esses pacotes teriam seus próprios repositórios reais.


## Fluxo de dados

![Arquitetura Favorite Products API](https://i.ibb.co/0CB28b7/Favorite-Produts-API.jpg)

Um Gateway centraliza a entrada e saída de requisições públicas. Valida token de acesso nos endpoints protegidos e roteia a requisição para o microsserviço adequado.

Pra acessar endpoints protegidos o usuário deve se autenticar e usar um token de acesso que dura 10 minutos. A validade do token é confirmada server side em uma chave no Redis com TTL igual ao do token de acesso.

O cadastro de cliente é feito através dos endpoints `/client` seguindo o padrão HTTP de métodos GET, POST e DELETE. Essas requisições são síncronas pela criticidade dos dados e feedback de validação instantânea.
Os dados de cliente e produtos favoritos são persistido no MongoDB e enviados através de filas para geradores de cache no Redis.
Por questões performance os dados jamais são lidos publicamente do MongoDB, mas sim do cache no Redis.

As requisições que incluem e removem produtos favoritos dos clientes não são síncronas. Vão para uma fila que será processada conforme disponibilidade. A única validação instantânea nessas requisições é o formato dos ids de cliente e produto. Pela baixa criticidade desses dados, caso haja algum erro no processamento a requisição falha silenciosamente. Idealmente essas falhas deveriam ir para uma dead letter queue ou fila de reprocessamento conforme regra de negócio.

A estrutura de microsserviços proposta pretende ser tolerante a falhas localizadas nos microsserviços e permite escalabilidade e economia de recursos.

## O que pode melhorar
Basicamente tudo (:

Mas algumas coisas específicas eu não consegui implementar pelo tempo disponível, mas gostaria de mencionar numa lista a fazer:

* Testes: escrever testes de integração, contrato e unitários. Tendo tempo para apenas um tipo escolhi os testes e2e. Na minha opinião os testes e2e tem valor destacado em arquiteturas de microsserviço - sem que os outros testes percam seu valor, claro.

* Gateway: colocar nele a camada de validação das requisições que acabou ficando nos microsserviços HTTP.

* Arquivos Docker: usar um Dockerfile para todos os microsserviços e abstrair em parâmetros as particularidades.

* Ambientes: separar desenvolvimento e teste.

* Autenticação e autorização: implementar OAuth 2.0 completo.

* GUID: trocar ObjectId por GUID na entidade de cliente.

Melhor eu parar por aqui porque a lista continuaria seguindo (: