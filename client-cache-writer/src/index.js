const { Redis } = require('@our-repo/databases')
const { errorHandler } = require('@our-repo/error-handler')

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const pub = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const channels = [
  'clientCreated',
  'clientUpdated',
  'clientRemoved',
  'favoriteProductAdded',
  'favoriteProductRemoved'
]

redis.subscribe(channels, (err) => {
  if (err) throw err
  try {
    redis.on('messageBuffer', async (channel, message) => {
      const stringChannel = channel.toString()
      const client = JSON.parse(message.toString())
      await pub.del(client._id)

      if (stringChannel !== 'clientRemoved') {
        await pub.sadd(client._id, JSON.stringify(client))
      }
    })
  } catch (e) {
    return errorHandler(e, null, { httpResponse: false })
  }
})
