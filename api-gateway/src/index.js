const express = require('express')
const { runEndpoints } = require('./runEndpoints')

const app = express()
app.use(express.json())

const endpoints = [
  {
    method: 'get',
    path: '/client/:clientId',
    baseUrl: `http://${process.env.SERVICE_CLIENT_READER_HOST}:${process.env.SERVICE_CLIENT_READER_PORT}`,
    closed: true
  },
  {
    method: 'post',
    path: '/client',
    baseUrl: `http://${process.env.SERVICE_CLIENT_CREATOR_HOST}:${process.env.SERVICE_CLIENT_CREATOR_PORT}`,
    closed: true
  },
  {
    method: 'post',
    path: '/client/:clientId',
    baseUrl: `http://${process.env.SERVICE_CLIENT_UPDATER_HOST}:${process.env.SERVICE_CLIENT_UPDATER_PORT}`,
    closed: true
  },
  {
    method: 'delete',
    path: '/client/:clientId',
    baseUrl: `http://${process.env.SERVICE_CLIENT_REMOVER_HOST}:${process.env.SERVICE_CLIENT_REMOVER_PORT}`,
    closed: true
  },
  {
    method: 'put',
    path: '/client/:clientId/favorite-product/:productId',
    baseUrl: `http://${process.env.SERVICE_FAVORITE_PRODUCT_QUEUER_HOST}:${process.env.SERVICE_FAVORITE_PRODUCT_QUEUER_PORT}`,
    closed: true
  },
  {
    method: 'delete',
    path: '/client/:clientId/favorite-product/:productId',
    baseUrl: `http://${process.env.SERVICE_FAVORITE_PRODUCT_QUEUER_HOST}:${process.env.SERVICE_FAVORITE_PRODUCT_QUEUER_PORT}`,
    closed: true
  },
  {
    method: 'post',
    path: '/login',
    baseUrl: `http://${process.env.SERVICE_AUTHENTICATOR_HOST}:${process.env.SERVICE_AUTHENTICATOR_PORT}`
  }
]

endpoints.map(endpoint => runEndpoints(app, endpoint))

app.listen(process.env.API_GATEWAY_PORT)
