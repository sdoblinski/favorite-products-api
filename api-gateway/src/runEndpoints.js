const axios = require('axios')
const { validate } = require('@our-repo/oauth')
const { errorHandler } = require('@our-repo/error-handler')
const { validateAuthorizedRequest } = validate

const runEndpoints = async (app, endpoint) => {
  const {
    path,
    method,
    baseUrl,
    closed
  } = endpoint

  app[method](path, async (req, res) => {
    try {
      if (closed) await validateAuthorizedRequest(req)

      let evalPath = path
      Object.keys(req.params).map((key) => {
        evalPath = evalPath.replace(`:${key}`, req.params[key])
      })

      const url = baseUrl + evalPath
      const { status, data } = await axios({
        method,
        url,
        data: req.body
      })

      res.status(status).send(data)
    } catch (e) {
      return errorHandler(e, res)
    }
  })
}

module.exports = {
  runEndpoints
}
