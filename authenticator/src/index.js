const express = require('express')
const createError = require('http-errors')
const { validate, accessToken } = require('@our-repo/oauth')
const { errorHandler } = require('@our-repo/error-handler')

const { validateLogin } = validate
const { generate } = accessToken

const app = express()
app.use(express.json())

const adminUser = 'admin'
const adminPassword = 'admin'
const data = { user: 'admin' }

app.post('/login', async (req, res) => {
  try {
    const { user, password } = validateLogin(req)
    if (user !== adminUser || password !== adminPassword) throw new createError.Unauthorized()

    const accessToken = await generate(data)

    return res.status(200).send({ accessToken })
  } catch (e) {
    return errorHandler(e, res)
  }
})

app.listen(process.env.SERVICE_AUTHENTICATOR_PORT)
