const express = require('express')
const { errorHandler } = require('@our-repo/error-handler')

const { create } = require('./client/create')

const app = express()
app.use(express.json())

app.post('/client', async (req, res) => {
  try {
    const client = await create(req)
    return res.status(201).send(client)
  } catch (e) {
    return errorHandler(e, res)
  }
})

app.listen(process.env.SERVICE_CLIENT_CREATOR_PORT)
