const { clientModel, validate } = require('@our-repo/client-entity')
const { mongo, Redis } = require('@our-repo/databases')

const { validateCreate } = validate
const Client = clientModel(mongo)

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const pub = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const create = async (req) => {
  const body = validateCreate(req)
  const newClient = new Client(body)
  await newClient.save()

  redis.subscribe('newClientReceived', (err) => {
    if (err) throw err
    pub.publish('clientCreated', JSON.stringify(newClient))
  })
  return newClient
}

module.exports = {
  create
}
