const express = require('express')
const { Redis } = require('@our-repo/databases')
const { validate } = require('@our-repo/favorite-product-entity')
const { validateRequest } = validate

const app = express()
app.use(express.json())

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const pub = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

app.put('/client/:clientId/favorite-product/:productId', async (req, res) => {
  try {
    const { clientId, productId } = validateRequest(req)
    const message = {
      clientId,
      productId,
      operation: 'add'
    }
    redis.subscribe('addFavoriteProductReceived', (err) => {
      if (err) throw err
      pub.publish('addFavoriteProductReceived', JSON.stringify(message))
      return res.status(200).send({ message: 'Favorite product successfully received' })
    })
  } catch (e) {
    console.error(e)
    if (e.statusCode && e.message) return res.status(e.statusCode).send({ message: e.message })
    return res.status(500).send({ message: 'Favorite product could not be received' })
  }
})

app.delete('/client/:clientId/favorite-product/:productId', async (req, res) => {
  try {
    const { clientId, productId } = validateRequest(req)
    const message = {
      clientId,
      productId,
      operation: 'remove'
    }
    redis.subscribe('removeFavoriteProductReceived', (err) => {
      if (err) throw err
      pub.publish('removeFavoriteProductReceived', JSON.stringify(message))
      return res.status(200).send({ message: 'Removed favorite product successfully received' })
    })
  } catch (e) {
    console.error(e)
    if (e.statusCode && e.message) return res.status(e.statusCode).send({ message: e.message })
    return res.status(500).send({ message: 'Removed favorite product could not be received' })
  }
})

app.listen(process.env.SERVICE_FAVORITE_PRODUCT_QUEUER_PORT)
