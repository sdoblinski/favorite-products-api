/**
 * @jest-environment node
 */
require('dotenv').config()
const { clientModel } = require('@our-repo/client-entity')
const { mongo } = require('@our-repo/databases')
const axios = require('axios')
const Client = clientModel(mongo)

const baseUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/client`
const loginUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/login`

const authenticationData = {
  user: 'admin',
  password: 'admin'
}

const clientData = {
  email: 'simeidoblinski@gmail.com',
  name: 'Simei',
}

let accessToken
let clientId

describe('Scenario: Remove Client', () => {
  beforeEach(async () => {
    await Client.deleteMany({})
    
    const { data:loginData } = await axios({
      method: 'post',
      url: loginUrl,
      data: authenticationData
    })
    accessToken = loginData.accessToken

    const { data:createData } = await axios({
      method: 'post',
      url: baseUrl,
      data: clientData,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })
    clientId = createData._id
  })

  it('Should receive status 200 and message in body when client id ok', async () => {
    const { status, data } = await axios({
      method: 'delete',
      url: `${baseUrl}/${clientId}`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    expect(status).toEqual(200)
    expect(data).toHaveProperty('message')
  })

  it('Should receive status 404 and message in body when trying to delete already deleted (non existent) client', async () => {
    try{
      await axios({
        method: 'delete',
        url: `${baseUrl}/${clientId}`,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(404)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 400 and message in body when using invalid objectId', async () => {
    try{
      await axios({
        method: 'delete',
        url: `${baseUrl}/1234567890`,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(400)
      expect(data).toHaveProperty('message')
    }
  })

})