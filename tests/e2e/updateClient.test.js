/**
 * @jest-environment node
 */
require('dotenv').config()
const { clientModel } = require('@our-repo/client-entity')
const { mongo } = require('@our-repo/databases')
const axios = require('axios')
const Client = clientModel(mongo)

const baseUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/client`
const loginUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/login`

const authenticationData = {
  user: 'admin',
  password: 'admin'
}

const clientData = {
  email: 'simeidoblinski@gmail.com',
  name: 'Simei',
}

const newClientData = {
  email: 'simeinovo@gmail.com',
  name: 'Simei Novo',
}

const invalidClientData = {
  email: 'email@errado',
  name: 'Simei',
}

const wrongClientPayload = {
  emails: 'email@errado',
  firstname: 'Simei',
}

let accessToken
let clientId

describe('Scenario: Update Client', () => {
  beforeAll(async () => {
    await Client.deleteMany({})

    const { data:loginData } = await axios({
      method: 'post',
      url: loginUrl,
      data: authenticationData
    })
    accessToken = loginData.accessToken

    const { data:createData } = await axios({
      method: 'post',
      url: baseUrl,
      data: clientData,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })
    clientId = createData._id
  })

  it('Should receive status 200 and client data in body when payload ok', async () => {
    const { status, data } = await axios({
      method: 'post',
      url: `${baseUrl}/${clientId}`,
      data: newClientData,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    expect(status).toEqual(200)
    expect(data).toHaveProperty('_id')
    expect(data).toHaveProperty('email', newClientData.email)
    expect(data).toHaveProperty('name', newClientData.name)
    expect(data).toHaveProperty('favoriteProducts')
    expect(data).toHaveProperty('createdAt')
    expect(data).toHaveProperty('updatedAt')
  })

  it('Should receive status 404 and message in body when trying to update non existent client', async () => {
    try{
      await axios({
        method: 'post',
        url: `${baseUrl}/5f04eac81c44bb001d820c79`,
        data: newClientData,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(404)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 400 and message in body when using invalid objectId', async () => {
    try {
      await axios({
        method: 'post',
        url: `${baseUrl}/123456789`,
        data: newClientData,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch(e) {
      const { status, data } = e.response
      expect(status).toEqual(400)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 400 and message in body when payload ok but invalid data', async () => {
    try {
      await axios({
        method: 'post',
        url: `${baseUrl}/123456789`,
        data: invalidClientData,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch(e) {
      const { status, data } = e.response
      expect(status).toEqual(400)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 400 and message in body when payload not ok', async () => {
    try {
      await axios({
        method: 'post',
        url: `${baseUrl}/123456789`,
        data: wrongClientPayload,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch(e) {
      const { status, data } = e.response
      expect(status).toEqual(400)
      expect(data).toHaveProperty('message')
    }
  })

})