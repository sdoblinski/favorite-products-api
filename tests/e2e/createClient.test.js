/**
 * @jest-environment node
 */
require('dotenv').config()
const { clientModel } = require('@our-repo/client-entity')
const { mongo } = require('@our-repo/databases')
const axios = require('axios')
const Client = clientModel(mongo)

const baseUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/client`
const loginUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/login`

const authenticationData = {
  user: 'admin',
  password: 'admin'
}

const clientData = {
  email: 'simeidoblinski@gmail.com',
  name: 'Simei',
}

const invalidClientData = {
  email: 'email@errado',
  name: 'Simei',
}

const wrongClientPayload = {
  emails: 'email@errado',
  firstname: 'Simei',
}

let accessToken

describe('Scenario: Create Client', () => {
  beforeAll(async () => {
    const { data } = await axios({
      method: 'post',
      url: loginUrl,
      data: authenticationData
    })
    accessToken = data.accessToken
  })

  it('Should receive status 201 and client data in body when payload ok', async () => {
    await Client.deleteMany({})
    const { status, data } = await axios({
      method: 'post',
      url: baseUrl,
      data: clientData,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    expect(status).toEqual(201)
    expect(data).toHaveProperty('_id')
    expect(data).toHaveProperty('email', clientData.email)
    expect(data).toHaveProperty('name', clientData.name)
    expect(data).toHaveProperty('favoriteProducts')
    expect(data).toHaveProperty('createdAt')
    expect(data).toHaveProperty('updatedAt')
  })

  it('Should receive status 400 and message in body when trying to create already existing client', async () => {
    await Client.deleteMany({})
    try {
        await axios({
        method: 'post',
        url: baseUrl,
        data: clientData,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch(e){

      const { status, data } = e.response
      expect(status).toEqual(400)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 400 and message in body when payload ok but invalid data', async () => {
    await Client.deleteMany({})
    try {
        await axios({
        method: 'post',
        url: baseUrl,
        data: invalidClientData,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })      
    } catch(e){

      const { status, data } = e.response
      expect(status).toEqual(400)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 400 and message in body when payload not ok', async () => {
    await Client.deleteMany({})
    try {
      await axios({
      method: 'post',
      url: baseUrl,
      data: wrongClientPayload,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })      
  } catch(e){
      
    const { status, data } = e.response
    expect(status).toEqual(400)
    expect(data).toHaveProperty('message')
  }
  })
})