/**
 * @jest-environment node
 */
require('dotenv').config()
const axios = require('axios')
const loginUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/login`
const clientUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/client`

const authenticationData = {
  user: 'admin',
  password: 'admin'
}

const invalidAuthenticationData = {
  user: 'user',
  password: 'password'
}

const wrongAuthenticationPayload = {
  usuario: 'user',
  senha: 'password'
}

const clientData = {
  email: 'simeidoblinski@gmail.com',
  name: 'Simei',
}

const expiredAccessToken = {
  Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXIiOiJhZG1pbiJ9LCJleHAiOjE1OTQxNzA4MDcsImlhdCI6MTU5NDE3MDc0N30.nRoHQpjpGOQgVlhduSIXYNaxx848DkcIqg9LJBWBum8'
}

describe('Scenario: Login', () => {

  it('Should receive status 200 and accessToken in body when payload ok and valid data', async () => {
    const { status, data } = await axios({
      method: 'post',
      url: loginUrl,
      data: authenticationData
    })

    expect(status).toEqual(200)
    expect(data).toHaveProperty('accessToken')
  })

  it('Should receive status 401 and message in body when payload ok and invalid data', async () => {
    try{
      await axios({
        method: 'post',
        url: loginUrl,
        data: invalidAuthenticationData
      })
    } catch(e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }

  })

  it('Should receive status 400 and message in body when when payload not ok', async () => {
    try{
      await axios({
        method: 'post',
        url: loginUrl,
        data: wrongAuthenticationPayload
      })
    } catch(e) {
      const { status, data } = e.response
      expect(status).toEqual(400)
      expect(data).toHaveProperty('message')
    }

  })
})

describe('Scenario: Unauthorized Endpoints', () => {

  it('Should receive status 401 and message in body when trying to create client without accessToken', async () => {
    try{
      await axios({
        method: 'post',
        url: clientUrl,
        data: clientData
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to update client without accessToken', async () => {
    try{
      await axios({
        method: 'post',
        url: `${clientUrl}/5f04eac81c44bb001d820c79`,
        data: clientData
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to read client without accessToken', async () => {
    try{
      await axios({
        method: 'get',
        url: `${clientUrl}/5f04eac81c44bb001d820c79`,
        data: clientData
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to remove client without accessToken', async () => {
    try{
      await axios({
        method: 'delete',
        url: `${clientUrl}/5f04eac81c44bb001d820c79`,
        data: clientData
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to add favorite product without accessToken', async () => {
    try{
      await axios({
        method: 'put',
        url: `${clientUrl}/5f04eac81c44bb001d820c79/favorite-product/130cc347-2119-7aa0-e23c-694bc5684861`,
        data: clientData,
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to remove favorite product without accessToken', async () => {
    try{
      await axios({
        method: 'delete',
        url: `${clientUrl}/5f04eac81c44bb001d820c79/favorite-product/130cc347-2119-7aa0-e23c-694bc5684861`,
        data: clientData,
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })
})

describe('Scenario: Access Token Expired', () => {

  it('Should receive status 401 and message in body when trying to create client with expired accessToken', async () => {
    try{
      await axios({
        method: 'post',
        url: clientUrl,
        data: clientData,
        headers: expiredAccessToken
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to update client with expired accessToken', async () => {
    try{
      await axios({
        method: 'post',
        url: `${clientUrl}/5f04eac81c44bb001d820c79`,
        data: clientData,
        headers: expiredAccessToken
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to read client with expired accessToken', async () => {
    try{
      await axios({
        method: 'get',
        url: `${clientUrl}/5f04eac81c44bb001d820c79`,
        data: clientData,
        headers: expiredAccessToken
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to remove client with expired accessToken', async () => {
    try{
      await axios({
        method: 'delete',
        url: `${clientUrl}/5f04eac81c44bb001d820c79`,
        data: clientData,
        headers: expiredAccessToken
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to add favorite product with expired accessToken', async () => {
    try{
      await axios({
        method: 'put',
        url: `${clientUrl}/5f04eac81c44bb001d820c79/favorite-product/130cc347-2119-7aa0-e23c-694bc5684861`,
        data: clientData,
        headers: expiredAccessToken
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })

  it('Should receive status 401 and message in body when trying to remove favorite product with expired accessToken', async () => {
    try{
      await axios({
        method: 'delete',
        url: `${clientUrl}/5f04eac81c44bb001d820c79/favorite-product/130cc347-2119-7aa0-e23c-694bc5684861`,
        data: clientData,
        headers: expiredAccessToken
      })
    } catch (e) {
      const { status, data } = e.response
      expect(status).toEqual(401)
      expect(data).toHaveProperty('message')
    }
  })
})