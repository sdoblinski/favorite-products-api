/**
 * @jest-environment node
 */
require('dotenv').config()
const { clientModel } = require('@our-repo/client-entity')
const { mongo } = require('@our-repo/databases')
const axios = require('axios')
const Client = clientModel(mongo)

const baseUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/client`
const loginUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/login`

const authenticationData = {
  user: 'admin',
  password: 'admin'
}

const clientData = {
  email: 'simeidoblinski@gmail.com',
  name: 'Simei',
}

const favoriteProductData = {
  'reviewScore':5.0,
  'title':'Jacques Bogart Silver Scent',
  'price':199.9,
  'brand':'jacques bogart',
  '_id':'130cc347-2119-7aa0-e23c-694bc5684861',
  'image':'http://challenge-api.luizalabs.com/images/130cc347-2119-7aa0-e23c-694bc5684861.jpg'
}

let accessToken
let clientId

describe('Scenario: Add Favorite Product', () => {
  beforeEach(async () => {
    await Client.deleteMany({})

    const { data:loginData } = await axios({
      method: 'post',
      url: loginUrl,
      data: authenticationData
    })
    accessToken = loginData.accessToken

    const { data:createData } = await axios({
      method: 'post',
      url: baseUrl,
      data: clientData,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })
    clientId = createData._id
  })

  it('Should receive status 200 and message in body when payload ok with data ok in get client', async (done) => {
    const { status, data } = await axios({
      method: 'put',
      url: `${baseUrl}/${clientId}/favorite-product/${favoriteProductData._id}`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    setTimeout( async () => {
      const { status:getStatus, data:getData } = await axios({
        method: 'get',
        url: `${baseUrl}/${clientId}`,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })

      const [ favoriteProduct ] = getData.favoriteProducts

      expect(status).toEqual(200)
      expect(data).toHaveProperty('message')
      expect(getStatus).toEqual(200)
      expect(favoriteProduct).toMatchObject(favoriteProductData)
      done()
    }, 500)
  })

  it('Should receive status 200 and message in body when payload ok with with no duplicate product when trying to add already added product', async (done) => {
    const { status:statusPut1, data:dataPut1 } = await axios({
      method: 'put',
      url: `${baseUrl}/${clientId}/favorite-product/${favoriteProductData._id}`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    const { status:statusPut2, data:dataPut2 } = await axios({
      method: 'put',
      url: `${baseUrl}/${clientId}/favorite-product/${favoriteProductData._id}`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    setTimeout( async () => {
      const { status:getStatus, data:getData } = await axios({
        method: 'get',
        url: `${baseUrl}/${clientId}`,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })

      const [ favoriteProduct ] = getData.favoriteProducts

      expect(statusPut1).toEqual(200)
      expect(dataPut1).toHaveProperty('message')
      expect(statusPut2).toEqual(200)
      expect(dataPut2).toHaveProperty('message')
      expect(getStatus).toEqual(200)
      expect(getData.favoriteProducts).toHaveProperty('length', 1)
      expect(favoriteProduct).toMatchObject(favoriteProductData)
      done()
    }, 500)
  })

  it('Should receive status 200 and message in body when payload ok with no product when trying to add non existent product', async (done) => {
    const { status, data } = await axios({
      method: 'put',
      url: `${baseUrl}/${clientId}/favorite-product/576d3d8e-accf-d751-99b4-a5740f4a4aa5`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    setTimeout( async () => {
      const { status:getStatus, data:getData } = await axios({
        method: 'get',
        url: `${baseUrl}/${clientId}`,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })
      
      expect(status).toEqual(200)
      expect(data).toHaveProperty('message')
      expect(getStatus).toEqual(200)
      expect(getData.favoriteProducts).toHaveProperty('length', 0)
      done()
    }, 500)
  })
})