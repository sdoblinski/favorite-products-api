/**
 * @jest-environment node
 */
require('dotenv').config()
const { clientModel } = require('@our-repo/client-entity')
const { mongo } = require('@our-repo/databases')
const axios = require('axios')
const Client = clientModel(mongo)

const baseUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/client`
const loginUrl = `http://localhost:${process.env.API_GATEWAY_PORT}/login`

const authenticationData = {
  user: 'admin',
  password: 'admin'
}

const clientData = {
  email: 'simeidoblinski@gmail.com',
  name: 'Simei',
}

const favoriteProductData = {
  'reviewScore':5.0,
  'title':'Jacques Bogart Silver Scent',
  'price':199.9,
  'brand':'jacques bogart',
  '_id':'130cc347-2119-7aa0-e23c-694bc5684861',
  'image':'http://challenge-api.luizalabs.com/images/130cc347-2119-7aa0-e23c-694bc5684861.jpg'
}

let accessToken
let clientId

describe('Scenario: Add Favorite Product', () => {
  beforeEach(async () => {
    await Client.deleteMany({})

    const { data:loginData } = await axios({
      method: 'post',
      url: loginUrl,
      data: authenticationData
    })
    accessToken = loginData.accessToken

    const { data:createData } = await axios({
      method: 'post',
      url: baseUrl,
      data: clientData,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })
    clientId = createData._id

    await axios({
      method: 'put',
      url: `${baseUrl}/${clientId}/favorite-product/${favoriteProductData._id}`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })    
  })

  it('Should receive status 200 and message and not product in client data', async (done) => {
    const { status:getStatus1, data:getData1 } = await axios({
      method: 'get',
      url: `${baseUrl}/${clientId}`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    const { status, data } = await axios({
      method: 'delete',
      url: `${baseUrl}/${clientId}/favorite-product/${favoriteProductData._id}`,
      headers: {'Authorization': `Bearer ${accessToken}`}
    })

    setTimeout( async () => {
      const { status:getStatus2, data:getData2 } = await axios({
        method: 'get',
        url: `${baseUrl}/${clientId}`,
        headers: {'Authorization': `Bearer ${accessToken}`}
      })

      expect(getStatus1).toEqual(200)
      expect(getData1.favoriteProducts).toHaveProperty('length', 1)
      expect(status).toEqual(200)
      expect(data).toHaveProperty('message')
      expect(getStatus2).toEqual(200)
      expect(getData2.favoriteProducts).toHaveProperty('length', 0)
      done()
    }, 500)
  })
})