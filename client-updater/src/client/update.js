const createError = require('http-errors')
const { clientModel, validate } = require('@our-repo/client-entity')
const { mongo, Redis } = require('@our-repo/databases')

const { validateUpdate } = validate
const Client = clientModel(mongo)

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const pub = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const update = async (req) => {
  const { params, body } = validateUpdate(req)
  const client = await Client.findByIdAndUpdate(
    params.clientId,
    body,
    {
      new: true
    })

  if (!client) throw createError.NotFound()

  redis.subscribe('clientUpdated', async (err) => {
    if (err) throw err
    pub.publish('clientUpdated', JSON.stringify(client))
  })

  return client
}

module.exports = {
  update
}
