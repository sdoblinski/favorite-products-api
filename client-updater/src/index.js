const express = require('express')
const { update } = require('./client/update')
const { errorHandler } = require('@our-repo/error-handler')

const app = express()
app.use(express.json())

app.post('/client/:clientId', async (req, res) => {
  try {
    const updatedClient = await update(req)
    return res.status(200).send(updatedClient)
  } catch (e) {
    return errorHandler(e, res)
  }
})

app.listen(process.env.SERVICE_CLIENT_UPDATER_PORT)
