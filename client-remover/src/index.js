const express = require('express')
const { errorHandler } = require('@our-repo/error-handler')

const { remove } = require('./client/remove')

const app = express()
app.use(express.json())

app.delete('/client/:clientId', async (req, res) => {
  try {
    await remove(req)
    return res.status(200).send({ message: 'Client successfully removed' })
  } catch (e) {
    return errorHandler(e, res)
  }
})

app.listen(process.env.SERVICE_CLIENT_REMOVER_PORT)
