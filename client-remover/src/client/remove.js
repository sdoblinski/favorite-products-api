const createError = require('http-errors')
const { clientModel, validate } = require('@our-repo/client-entity')
const { mongo, Redis } = require('@our-repo/databases')

const { validateRemove } = validate
const Client = clientModel(mongo)

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const pub = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const remove = async (req) => {
  const { clientId } = validateRemove(req)

  const client = await Client.findByIdAndDelete(clientId)

  if (!client) throw createError.NotFound()

  redis.subscribe('clientRemoved', async (err) => {
    if (err) throw err
    pub.publish('clientRemoved', JSON.stringify(client))
  })
}

module.exports = {
  remove
}
