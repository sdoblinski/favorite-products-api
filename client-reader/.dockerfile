FROM node:alpine
RUN apk add --no-cache git
RUN apk add --no-cache openssh
COPY ./client-reader .
COPY ./common ./common
RUN npm install
RUN npm install pm2 -g
CMD ["npm", "start"]