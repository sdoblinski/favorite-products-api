const express = require('express')
const createError = require('http-errors')
const { validate } = require('@our-repo/client-entity')
const { redisConnection } = require('@our-repo/databases')
const { errorHandler } = require('@our-repo/error-handler')

const redis = redisConnection()

const { validateRead } = validate

const app = express()
app.use(express.json())

app.get('/client/:clientId', async (req, res) => {
  try {
    const { clientId } = validateRead(req)

    const [client] = await redis.smembers(clientId)
    if (client) return res.status(200).send(client)
    throw createError.NotFound()
  } catch (e) {
    return errorHandler(e, res)
  }
})

app.listen(process.env.SERVICE_CLIENT_READER_PORT)
