const createError = require('http-errors')

const mongoErrorHandler = (e) => {
  if (e.code === 11000) {
    const message = 'This information already exists'
    return createError(400, message)
  }
  console.error(e)
  return e
}

module.exports = {
  mongoErrorHandler
}
