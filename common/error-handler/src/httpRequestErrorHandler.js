const createError = require('http-errors')

const httpRequestErrorHandler = (e) => {
  return createError(e.response.status, e.response.data)
}

module.exports = {
  httpRequestErrorHandler
}
