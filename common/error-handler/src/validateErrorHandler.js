const createError = require('http-errors')

const validateErrorHandler = (e) => {
  const messages = e.details
    .map(detail => detail.message)
    .join(', ')
    .replace(/"/g, '\'')
  return createError(400, messages)
}

module.exports = {
  validateErrorHandler
}
