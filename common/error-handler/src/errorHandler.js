const { validateErrorHandler } = require('./validateErrorHandler')
const { mongoErrorHandler } = require('./mongoErrorHandler')
const { httpRequestErrorHandler } = require('./httpRequestErrorHandler')

const errorHandler = (e, res, options = {}) => {
  try {
    if (e.statusCode && e.message) return res.status(e.statusCode).send({ message: e.message })

    if (e.details) throw validateErrorHandler(e)
    if (e.name === 'MongoError') throw mongoErrorHandler(e)
    if (e.isAxiosError) throw httpRequestErrorHandler(e)
    throw e
  } catch (error) {
    if (options.httpResponse === false) return console.error(error)

    if (error.statusCode && error.message) return res.status(error.statusCode).send({ message: error.message })
    console.error(error)
    return res.status(500).send('Internal Server Error')
  }
}

module.exports = {
  errorHandler
}
