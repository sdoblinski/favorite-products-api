const mongoose = require('mongoose')
mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
mongoose.set('useUnifiedTopology', true)

const mongo = mongoose.createConnection(process.env.MONGODB_URI)

module.exports = {
  mongo
}
