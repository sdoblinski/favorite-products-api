const Redis = require('ioredis')

const redisConnection = () => {
  return new Redis({
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST
  })
}

module.exports = {
  redisConnection,
  Redis
}
