const { mongo } = require('./mongo')
const { redisConnection, Redis } = require('./redis')

module.exports = {
  mongo,
  redisConnection,
  Redis
}
