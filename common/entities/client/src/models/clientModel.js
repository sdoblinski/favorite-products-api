const { clientSchema } = require('../schemas/clientSchema')

const clientModel = (mongo) => mongo.model('Client', clientSchema)

module.exports = {
  clientModel
}
