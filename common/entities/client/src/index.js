const { clientModel } = require('./models/clientModel.js')

const { validateCreate } = require('./validate/validateCreate')
const { validateRead } = require('./validate/validateRead')
const { validateRemove } = require('./validate/validateRemove')
const { validateUpdate } = require('./validate/validateUpdate')

const { clientSchema } = require('./schemas/clientSchema')
const { createSchema } = require('./schemas/validate/createSchema')
const { readSchema } = require('./schemas/validate/readSchema')
const { removeSchema } = require('./schemas/validate/removeSchema')
const { updateSchema } = require('./schemas/validate/updateSchema')

module.exports = {
  clientModel,
  validate: {
    validateCreate,
    validateRead,
    validateRemove,
    validateUpdate
  },
  schemas: {
    clientSchema,
    createSchema,
    readSchema,
    removeSchema,
    updateSchema
  }
}
