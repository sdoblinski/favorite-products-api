const { updateSchema } = require('../schemas/validate/updateSchema')
const createError = require('http-errors')

const validateUpdate = (req) => {
  const { error: e, value } = updateSchema.validate({ params: req.params, body: req.body }, { abortEarly: false })
  if (e) {
    const messages = e.details
      .map(detail => detail.message)
      .join(', ')
      .replace(/"/g, '\'')
    throw createError.BadRequest(messages)
  }
  return value
}

module.exports = {
  validateUpdate
}
