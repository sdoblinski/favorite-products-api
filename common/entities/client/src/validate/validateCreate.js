const { createSchema } = require('../schemas/validate/createSchema')
const createError = require('http-errors')

const validateCreate = (req) => {
  const { error: e, value } = createSchema.validate(req.body, { abortEarly: false })
  if (e) {
    const messages = e.details
      .map(detail => detail.message)
      .join(', ')
      .replace(/"/g, '\'')
    throw createError.BadRequest(messages)
  }
  return value
}

module.exports = {
  validateCreate
}
