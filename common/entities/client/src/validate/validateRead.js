const { readSchema } = require('../schemas/validate/readSchema')
const createError = require('http-errors')

const validateRead = (req) => {
  const { error, value } = readSchema.validate(req.params, { abortEarly: false })
  if (error) {
    const messages = error.details
      .map(detail => detail.message)
      .join(', ')
      .replace(/"/g, '\'')
    throw createError.BadRequest(messages)
  }
  return value
}

module.exports = {
  validateRead
}
