const { removeSchema } = require('../schemas/validate/removeSchema')
const createError = require('http-errors')

const validateRemove = (req) => {
  const { error: e, value } = removeSchema.validate(req.params, { abortEarly: false })
  if (e) {
    const messages = e.details
      .map(detail => detail.message)
      .join(', ')
      .replace(/"/g, '\'')
    throw createError.BadRequest(messages)
  }
  return value
}

module.exports = {
  validateRemove
}
