const { Schema } = require('mongoose')
const { schemas } = require('@our-repo/favorite-product-entity')
const { favoriteProductSchema } = schemas

const clientSchema = new Schema({
  email: {
    type: String,
    unique: true,
    require: true,
    maxlength: 100
  },
  name: {
    type: String,
    maxlength: 100,
    require: true
  },
  favoriteProducts: [favoriteProductSchema]
},
{
  timestamps: true
})

module.exports = {
  clientSchema
}
