const Joi = require('@hapi/joi')

const readSchema = module.exports = Joi.object({
  clientId: Joi.string().pattern(/^[a-f\d]{24}$/i).required()
}).unknown(false)

module.exports = {
  readSchema
}
