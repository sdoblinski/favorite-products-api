const Joi = require('@hapi/joi')

const createSchema = Joi.object({
  email: Joi.string().max(100).email().required(),
  name: Joi.string().max(100).required()
}).unknown(false)

module.exports = {
  createSchema
}
