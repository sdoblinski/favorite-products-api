const Joi = require('@hapi/joi')

const updateSchema = Joi.object({
  params: Joi.object({
    clientId: Joi.string().pattern(/^[a-f\d]{24}$/i).required()
  }).unknown(false),
  body: Joi.object({
    email: Joi.string().max(100).email().required(),
    name: Joi.string().max(100).required()
  }).unknown(false)
})

module.exports = {
  updateSchema
}
