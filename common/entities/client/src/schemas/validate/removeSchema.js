const Joi = require('@hapi/joi')

const removeSchema = Joi.object({
  clientId: Joi.string().pattern(/^[a-f\d]{24}$/i).required()
}).unknown(false)

module.exports = {
  removeSchema
}
