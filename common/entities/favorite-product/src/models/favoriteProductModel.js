const { favoriteProductSchema } = require('../schemas/favoriteProductSchema')

const favoriteProductModel = (mongo) => mongo.model('FavoriteProduct', favoriteProductSchema)

module.exports = {
  favoriteProductModel
}
