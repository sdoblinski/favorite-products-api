const { requestSchema } = require('../schemas/validate/requestSchema')
const createError = require('http-errors')

const validateRequest = (req) => {
  const { error: e, value } = requestSchema.validate(req.params, { abortEarly: false })
  if (e) {
    const messages = e.details
      .map(detail => detail.message)
      .join(', ')
      .replace(/"/g, '\'')
    throw createError.BadRequest(messages)
  }
  return value
}

module.exports = {
  validateRequest
}
