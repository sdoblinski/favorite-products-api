const { productSchema } = require('../schemas/validate/productSchema')

const validateProduct = (product) => {
  const { error, value } = productSchema.validate(product, { abortEarly: false })
  if (error) throw error
  return value
}

module.exports = {
  validateProduct
}
