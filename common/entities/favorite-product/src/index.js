const { favoriteProductModel } = require('./models/favoriteProductModel')
const { favoriteProductSchema } = require('./schemas/favoriteProductSchema.js')
const { validateRequest } = require('./validate/validateRequest')
const { validateProduct } = require('./validate/validateProduct')

module.exports = {
  favoriteProductModel,
  validate: {
    validateRequest,
    validateProduct
  },
  schemas: {
    favoriteProductSchema
  }
}
