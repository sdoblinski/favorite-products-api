const Joi = require('@hapi/joi')

const requestSchema = Joi.object({
  clientId: Joi.string().pattern(/^[a-f\d]{24}$/i).required(),
  productId: Joi.string().guid().required()
}).unknown(false)

module.exports = {
  requestSchema
}
