const Joi = require('@hapi/joi')

const productSchema = Joi.object({
  _id: Joi.string().guid().required(),
  title: Joi.string().max(100).required(),
  brand: Joi.string().max(100).required(),
  image: Joi.string().max(200).required(),
  price: Joi.number().max(9999999999).required(),
  reviewScore: Joi.number().max(9999999999)
}).unknown(false)

module.exports = {
  productSchema
}
