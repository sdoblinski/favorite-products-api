const { Schema } = require('mongoose')

const favoriteProductSchema = Schema({
  _id: {
    type: String,
    required: true
  },
  title: {
    type: String,
    require: true,
    maxlength: 100
  },
  brand: {
    type: String,
    require: true,
    maxlength: 100
  },
  image: {
    type: String,
    require: true,
    maxlength: 200
  },
  price: {
    type: Number,
    require: true,
    maxlength: 10
  },
  reviewScore: {
    type: Number,
    maxlength: 10
  }
})

module.exports = {
  favoriteProductSchema
}
