const Joi = require('@hapi/joi')

const loginSchema = Joi.object({
  user: Joi.string().max(50).required(),
  password: Joi.string().max(50).required()
}).unknown(false)

module.exports = {
  loginSchema
}
