const Joi = require('@hapi/joi')

const authorizedRequestSchema = Joi.object({
  authorization: Joi.string().required()
}).unknown(true)

module.exports = {
  authorizedRequestSchema
}
