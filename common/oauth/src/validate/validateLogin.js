const { loginSchema } = require('../schemas/loginSchema')
const createError = require('http-errors')

const validateLogin = (req) => {
  const { error: e, value } = loginSchema.validate(req.body, { abortEarly: false })
  if (e) {
    const messages = e.details
      .map(detail => detail.message)
      .join(', ')
      .replace(/"/g, '\'')
    throw createError.BadRequest(messages)
  }
  return value
}

module.exports = {
  validateLogin
}
