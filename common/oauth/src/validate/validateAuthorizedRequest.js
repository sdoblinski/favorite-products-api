const { Redis } = require('@our-repo/databases')
const { authorizedRequestSchema } = require('../schemas/authorizedRequestSchema')
const { validateToken } = require('../validate/validateToken')
const createError = require('http-errors')

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})
const validateAuthorizedRequest = async (req) => {
  try {
    const result = authorizedRequestSchema.validate(req.headers, { abortEarly: false })
    const { error, value } = result
    const authorization = value.authorization.split(' ')
    const [bearer, accessToken] = authorization
    if (error || bearer !== 'Bearer') throw createError.Unauthorized()

    validateToken(accessToken)
    const hasToken = await redis.get(accessToken)
    if (!hasToken) throw createError.Unauthorized()
  } catch (e) {
    throw createError.Unauthorized()
  }
}

module.exports = {
  validateAuthorizedRequest
}
