const jwt = require('jsonwebtoken')
const secret = process.env.OAUTH_SECRET

const validateToken = (token) => {
  return jwt.verify(token, secret)
}

module.exports = {
  validateToken
}
