const { validateLogin } = require('./validate/validateLogin')
const { validateToken } = require('./validate/validateToken')
const { validateAuthorizedRequest } = require('./validate/validateAuthorizedRequest')
const accessToken = require('./tokens/accessToken')

module.exports = {
  accessToken,
  validate: {
    validateLogin,
    validateToken,
    validateAuthorizedRequest
  }
}
