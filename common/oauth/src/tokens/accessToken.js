const jwt = require('jsonwebtoken')
const { redisConnection } = require('@our-repo/databases')

const redis = redisConnection()
const secret = process.env.OAUTH_SECRET

const generate = async (data) => {
  const ttl = 60 * process.env.ACCESS_TOKEN_TTL_MINUTES

  const accessToken = jwt.sign({
    data,
    exp: Math.floor(Date.now() / 1000) + ttl
  }, secret)

  await redis.set(accessToken, 1)
  await redis.expire(accessToken, ttl)

  return accessToken
}

module.exports = {
  generate
}
