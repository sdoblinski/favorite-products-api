const { clientModel } = require('@our-repo/client-entity')
const { mongo, Redis } = require('@our-repo/databases')

const Client = clientModel(mongo)

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const pub = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const remove = async (clientId, productId) => {
  const client = await Client.findOneAndUpdate(
    { _id: clientId },
    {
      $pull: {
        favoriteProducts: {
          _id: productId
        }
      }
    },
    { new: true }
  )

  redis.subscribe('favoriteProductRemoved', (err) => {
    if (err) throw err
    pub.publish('favoriteProductRemoved', JSON.stringify(client))
  })
}

module.exports = {
  remove
}
