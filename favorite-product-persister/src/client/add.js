const { favoriteProductModel, validate } = require('@our-repo/favorite-product-entity')
const { clientModel } = require('@our-repo/client-entity')
const { mongo, Redis } = require('@our-repo/databases')
// const axios = require('axios')
const { getProduct } = require('../product/getProduct')
const { validateProduct } = validate

const Client = clientModel(mongo)
const FavoriteProduct = favoriteProductModel(mongo)

const redis = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const pub = new Redis({
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST
})

const add = async (clientId, productId, operation) => {
  // const url = `http://challenge-api.luizalabs.com/api/product/${productId}`
  // const { data } = await axios.get(url)

  const data = getProduct(productId)
  if (!data) return

  const {
    id: _id, ...product
  } = data
  product._id = _id

  const body = validateProduct(product)
  const newFavoriteProduct = new FavoriteProduct(body)

  const client = await Client.findOneAndUpdate(
    { _id: clientId },
    { $addToSet: { favoriteProducts: newFavoriteProduct } },
    { new: true }
  )

  redis.subscribe('favoriteProductAdded', (err) => {
    if (err) throw err
    pub.publish('favoriteProductAdded', JSON.stringify(client))
  })
}

module.exports = {
  add
}
