const products = require('./products')

const getProduct = (productId) => {
  return products.find((product) => product.id === productId)
}

module.exports = {
  getProduct
}
