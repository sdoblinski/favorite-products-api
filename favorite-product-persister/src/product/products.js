module.exports = [
  {
    price: 189.0,
    image: 'http://challenge-api.luizalabs.com/images/3a5fe8b8-0366-d573-8e14-bc96828fe15c.jpg',
    brand: 'artex',
    id: '3a5fe8b8-0366-d573-8e14-bc96828fe15c',
    title: 'Kit Colcha/Cobre-Leito Solteiro Artex Pixel Percal'
  },
  {
    price: 1099.0,
    image: 'http://challenge-api.luizalabs.com/images/576d2d8e-accf-d758-99b4-a5740f4a4a35.jpg',
    brand: 'galzerano',
    id: '576d2d8e-accf-d758-99b4-a5740f4a4a35',
    title: 'Carrinho de Beb\u00ea Passeio Galzerano Athena'
  },
  {
    price: 139.9,
    image: 'http://challenge-api.luizalabs.com/images/39bbf5ce-ffb0-6f40-ac3c-dc36bd8ce4b4.jpg',
    brand: 'sportpharma',
    id: '39bbf5ce-ffb0-6f40-ac3c-dc36bd8ce4b4',
    title: 'Amino Max 160 Tabletes'
  },
  {
    price: 269.0,
    image: 'http://challenge-api.luizalabs.com/images/117341cc-5d55-6979-fbcf-4a0631da082a.jpg',
    brand: 'galzerano',
    id: '117341cc-5d55-6979-fbcf-4a0631da082a',
    title: 'Beb\u00ea Conforto Galzerano Piccolina'
  },
  {
    reviewScore: 4.0,
    title: 'Thermo Fire 14 C\u00e1psulas',
    price: 45.9,
    brand: 'arnold nutrition',
    id: '0f950098-2a4b-134b-4a8e-20fcd9b53a22',
    image: 'http://challenge-api.luizalabs.com/images/0f950098-2a4b-134b-4a8e-20fcd9b53a22.jpg'
  },
  {
    price: 1099.0,
    image: 'http://challenge-api.luizalabs.com/images/73e057d8-d432-aac1-b563-9c27bf85ff47.jpg',
    brand: 'galzerano',
    id: '73e057d8-d432-aac1-b563-9c27bf85ff47',
    title: 'Carrinho Ber\u00e7o e de Passeio Galzerano Athena'
  },
  {
    price: 256.0,
    image: 'http://challenge-api.luizalabs.com/images/ecd4676e-41d1-7bea-e0d6-880ef82e7421.jpg',
    brand: 'artex',
    id: 'ecd4676e-41d1-7bea-e0d6-880ef82e7421',
    title: 'Kit Colcha/Cobre-Leito King Size Artex Percal'
  },
  {
    price: 211.9,
    image: 'http://challenge-api.luizalabs.com/images/ce7f3896-b893-5442-dcd4-98502fb7506c.jpg',
    brand: 'sportpharma',
    id: 'ce7f3896-b893-5442-dcd4-98502fb7506c',
    title: 'Amino Max 325 Tabletes'
  },
  {
    price: 129.9,
    image: 'http://challenge-api.luizalabs.com/images/01c4f0c5-96db-6a5a-3d65-e0a3e7bb7206.jpg',
    brand: 'philco',
    id: '01c4f0c5-96db-6a5a-3d65-e0a3e7bb7206',
    title: 'Ventilador de Mesa e Parede Philco'
  },
  {
    price: 1179.0,
    image: 'http://challenge-api.luizalabs.com/images/d1891ff5-8b26-a94d-2763-8873dd20b01f.jpg',
    brand: 'completa m\u00f3veis',
    id: 'd1891ff5-8b26-a94d-2763-8873dd20b01f',
    title: 'Conjunto Infantil Ninar'
  },
  {
    price: 272.9,
    image: 'http://challenge-api.luizalabs.com/images/c7a387f1-b2fd-dbb7-823c-44db33c2e218.jpg',
    brand: 'optimum nutrition',
    id: 'c7a387f1-b2fd-dbb7-823c-44db33c2e218',
    title: 'BCAA 1000 400 c\u00e1psulas Mega Size'
  },
  {
    price: 1179.0,
    image: 'http://challenge-api.luizalabs.com/images/5984e4af-a0e1-10c4-16d8-4edb2685117f.jpg',
    brand: 'completa m\u00f3veis',
    id: '5984e4af-a0e1-10c4-16d8-4edb2685117f',
    title: 'Conjunto Infantil Ninar'
  },
  {
    price: 699.0,
    image: 'http://challenge-api.luizalabs.com/images/38c657d3-b591-eacc-416f-fa2ed1910391.jpg',
    brand: 'tec toy',
    id: '38c657d3-b591-eacc-416f-fa2ed1910391',
    title: 'Bab\u00e1 Eletr\u00f4nica Tec Toy Baby Care VBT-2400'
  },
  {
    price: 219.0,
    image: 'http://challenge-api.luizalabs.com/images/8d31fa04-18f9-910a-1931-b7b04cb4484e.jpg',
    brand: 'galzerano',
    id: '8d31fa04-18f9-910a-1931-b7b04cb4484e',
    title: 'Beb\u00ea Conforto Galzerano Piccolina'
  },
  {
    price: 198.0,
    image: 'http://challenge-api.luizalabs.com/images/1fb3ad4b-ebd6-3830-b7cc-99c533c26005.jpg',
    brand: 'jacques bogart',
    id: '1fb3ad4b-ebd6-3830-b7cc-99c533c26005',
    title: 'Jacques Bogart Riviera Nights'
  },
  {
    price: 139.9,
    image: 'http://challenge-api.luizalabs.com/images/29b7397d-d0ee-7f3d-3f78-f407ec67450b.jpg',
    brand: 'elgin',
    id: '29b7397d-d0ee-7f3d-3f78-f407ec67450b',
    title: 'Antena GSM Dual Band'
  },
  {
    price: 52.0,
    image: 'http://challenge-api.luizalabs.com/images/30e0c8a9-cfeb-6a92-8c5f-8db4af903bfd.jpg',
    brand: 'n.p.p.e.',
    id: '30e0c8a9-cfeb-6a92-8c5f-8db4af903bfd',
    title: 'Blush Compacto Cor 33 Lil\u00e1s'
  },
  {
    price: 59.9,
    image: 'http://challenge-api.luizalabs.com/images/71f17c3a-31eb-7b91-7896-9417507aaba6.jpg',
    brand: 'elgin',
    id: '71f17c3a-31eb-7b91-7896-9417507aaba6',
    title: 'Telefone com Fio Elgin'
  },
  {
    price: 155.9,
    image: 'http://challenge-api.luizalabs.com/images/59f65835-1cdf-3777-5f2e-6ac42e46305d.jpg',
    brand: 'jacques bogart',
    id: '59f65835-1cdf-3777-5f2e-6ac42e46305d',
    title: 'Jacques Bogart Riviera Nights'
  },
  {
    price: 1179.0,
    image: 'http://challenge-api.luizalabs.com/images/da48843c-8b6c-7d81-f9bb-999a09250d7b.jpg',
    brand: 'completa m\u00f3veis',
    id: 'da48843c-8b6c-7d81-f9bb-999a09250d7b',
    title: 'Conjunto Infantil Ninar'
  },
  {
    price: 209.9,
    image: 'http://challenge-api.luizalabs.com/images/9e9a991a-63c2-347a-2d0f-f20c7f692a44.jpg',
    brand: 'jacques bogart',
    id: '9e9a991a-63c2-347a-2d0f-f20c7f692a44',
    title: 'Jacques Bogart Silver Scent Intense'
  },
  {
    price: 59.0,
    image: 'http://challenge-api.luizalabs.com/images/655e6e10-9cc7-5bbf-5956-df16063bb22c.jpg',
    brand: 'elgin',
    id: '655e6e10-9cc7-5bbf-5956-df16063bb22c',
    title: 'Telefone com Fio Elgin'
  },
  {
    reviewScore: 5.0,
    title: 'Jacques Bogart Silver Scent',
    price: 199.9,
    brand: 'jacques bogart',
    id: '130cc347-2119-7aa0-e23c-694bc5684861',
    image: 'http://challenge-api.luizalabs.com/images/130cc347-2119-7aa0-e23c-694bc5684861.jpg'
  },
  {
    price: 2699.0,
    image: 'http://challenge-api.luizalabs.com/images/ff8adeed-4db0-04df-070f-4a13089a811d.jpg',
    brand: 'tramontina',
    id: 'ff8adeed-4db0-04df-070f-4a13089a811d',
    title: 'Coifa de Parede Tramontina New Vetro Wall Flat'
  },
  {
    price: 159.9,
    image: 'http://challenge-api.luizalabs.com/images/3e0dba33-94b9-b275-92c9-cf64f7831132.jpg',
    brand: 'molyneux',
    id: '3e0dba33-94b9-b275-92c9-cf64f7831132',
    title: 'Molyneux Quartz Silver pour Homme'
  },
  {
    price: 38.0,
    image: 'http://challenge-api.luizalabs.com/images/ebecba6f-f61a-c878-cf8d-8cb8ccd865ec.jpg',
    brand: 'charis',
    id: 'ebecba6f-f61a-c878-cf8d-8cb8ccd865ec',
    title: 'Condicionador Ortomolecular para os Cabelos'
  },
  {
    reviewScore: 4.0,
    title: 'ZMA Power 90 C\u00e1psulas',
    price: 98.9,
    brand: 'probi\u00f3tica',
    id: 'd15a3ad9-f7a8-3c5d-937f-0b4144e5f553',
    image: 'http://challenge-api.luizalabs.com/images/d15a3ad9-f7a8-3c5d-937f-0b4144e5f553.jpg'
  },
  {
    price: 1799.0,
    image: 'http://challenge-api.luizalabs.com/images/f6c9dbfa-7014-c5b5-88b4-38974f262999.jpg',
    brand: 'tramontina',
    id: 'f6c9dbfa-7014-c5b5-88b4-38974f262999',
    title: 'Pia Fog\u00e3o 2 Bocas Tramontina 93712 de Embutir Inox'
  },
  {
    price: 38.0,
    image: 'http://challenge-api.luizalabs.com/images/da6fa735-392a-96a6-1c0e-4cfa13614f69.jpg',
    brand: 'charis',
    id: 'da6fa735-392a-96a6-1c0e-4cfa13614f69',
    title: 'Condicionador para os Cabelos Violet Desamarelador'
  },
  {
    price: 1499.9,
    image: 'http://challenge-api.luizalabs.com/images/1182569d-aaf5-87aa-04ba-f2449637bac0.jpg',
    brand: 'kitchenaid',
    id: '1182569d-aaf5-87aa-04ba-f2449637bac0',
    title: 'Processador de Alimentos KitchenAid'
  },
  {
    price: 1799.0,
    image: 'http://challenge-api.luizalabs.com/images/34f94377-6cb8-82e5-93a3-b0c98ee28fb1.jpg',
    brand: 'tramontina',
    id: '34f94377-6cb8-82e5-93a3-b0c98ee28fb1',
    title: 'Pia Fog\u00e3o 2 Bocas Tramontina 93710 de Embutir Inox'
  },
  {
    price: 19.9,
    image: 'http://challenge-api.luizalabs.com/images/d7ebcf96-286d-3736-8cee-e519dbd7d0fc.jpg',
    brand: 'biona cer\u00e2mica',
    id: 'd7ebcf96-286d-3736-8cee-e519dbd7d0fc',
    title: 'Tigela em Cer\u00e2mica'
  },
  {
    price: 42.9,
    image: 'http://challenge-api.luizalabs.com/images/5f1c41e5-c700-080b-c1a0-804b5ba849ca.jpg',
    brand: 'bourjois',
    id: '5f1c41e5-c700-080b-c1a0-804b5ba849ca',
    title: 'Gloss Labial Effet 3D Cor 04 Rose Polemic'
  },
  {
    price: 37.9,
    image: 'http://challenge-api.luizalabs.com/images/0d990c3b-b1bd-f5ee-f74e-29a6c49a2e62.jpg',
    brand: 'bourjois',
    id: '0d990c3b-b1bd-f5ee-f74e-29a6c49a2e62',
    title: 'Gloss Labial Effet 3D Cor 20 Rose Symphonic'
  },
  {
    price: 549.9,
    image: 'http://challenge-api.luizalabs.com/images/3149e04d-da2f-c2c8-d2c1-ae499f7cdd31.jpg',
    brand: 'kitchenaid',
    id: '3149e04d-da2f-c2c8-d2c1-ae499f7cdd31',
    title: 'Mini Processador de Alimentos KitchenAid'
  },
  {
    price: 38.7,
    image: 'http://challenge-api.luizalabs.com/images/f606a417-eee3-24b1-e48b-20ae8dccaae8.jpg',
    brand: 'charis',
    id: 'f606a417-eee3-24b1-e48b-20ae8dccaae8',
    title: 'Leave in Ortomolecular Professional 250 ml'
  },
  {
    price: 549.9,
    image: 'http://challenge-api.luizalabs.com/images/ec38c709-582e-8c33-4cad-93f799094eb4.jpg',
    brand: 'kitchenaid',
    id: 'ec38c709-582e-8c33-4cad-93f799094eb4',
    title: 'Mini Processador de Alimentos KitchenAid'
  },
  {
    price: 129.0,
    image: 'http://challenge-api.luizalabs.com/images/c61225bd-fb77-b575-4918-b95e85f9079b.jpg',
    brand: 'arnold nutrition',
    id: 'c61225bd-fb77-b575-4918-b95e85f9079b',
    title: 'ZMA 12 c\u00e1psulas'
  },
  {
    price: 1099.9,
    image: 'http://challenge-api.luizalabs.com/images/8e8cc711-9048-4cf7-2d34-4c377bcc17cc.jpg',
    brand: 'kitchenaid',
    id: '8e8cc711-9048-4cf7-2d34-4c377bcc17cc',
    title: 'Processador de Alimentos KitchenAid'
  },
  {
    price: 189.0,
    image: 'http://challenge-api.luizalabs.com/images/9ba2bfa9-be9c-a22e-03e6-7a12876b5603.jpg',
    brand: 'arnold nutrition',
    id: '9ba2bfa9-be9c-a22e-03e6-7a12876b5603',
    title: 'HGH IGF-1 30.000 120 ml Sublingual'
  },
  {
    price: 549.9,
    image: 'http://challenge-api.luizalabs.com/images/e7449c83-20c7-7754-a4a8-fc7addd86972.jpg',
    brand: 'kitchenaid',
    id: 'e7449c83-20c7-7754-a4a8-fc7addd86972',
    title: 'Mini Processador de Alimentos KitchenAid'
  },
  {
    price: 1799.0,
    image: 'http://challenge-api.luizalabs.com/images/e5fd75f7-54da-1481-cf35-08dd62c10c99.jpg',
    brand: 'tramontina',
    id: 'e5fd75f7-54da-1481-cf35-08dd62c10c99',
    title: 'Pia Fog\u00e3o 2 Bocas Tramontina 93720 de Embutir Inox'
  },
  {
    reviewScore: 5.0,
    title: 'The Elder Scrolls V Skyrim para PS3',
    price: 79.9,
    brand: 'bethesda',
    id: '1df1a194-28dd-ef0b-5c0b-6ea44061aae5',
    image: 'http://challenge-api.luizalabs.com/images/1df1a194-28dd-ef0b-5c0b-6ea44061aae5.jpg'
  },
  {
    price: 122.9,
    image: 'http://challenge-api.luizalabs.com/images/e3a1cc09-de87-93ce-18b6-cb3f144b8d0f.jpg',
    brand: 'arnold nutrition',
    id: 'e3a1cc09-de87-93ce-18b6-cb3f144b8d0f',
    title: 'Theburner 120 C\u00e1psulas'
  },
  {
    price: 249.9,
    image: 'http://challenge-api.luizalabs.com/images/64c21ba3-1b24-babe-d1f1-c181cfeb6312.jpg',
    brand: 'logitech',
    id: '64c21ba3-1b24-babe-d1f1-c181cfeb6312',
    title: 'Auto-falantes 25RMS 2.1 Canais Subwoofer'
  },
  {
    price: 2199.0,
    image: 'http://challenge-api.luizalabs.com/images/844e9392-9a30-8e12-f537-ae8705c0e41c.jpg',
    brand: 'tramontina',
    id: '844e9392-9a30-8e12-f537-ae8705c0e41c',
    title: 'Pia Fog\u00e3o 2 Bocas Tramontina 93713 El\u00e9trico Inox'
  },
  {
    price: 2349.0,
    image: 'http://challenge-api.luizalabs.com/images/870eca11-8ac4-f765-f9a5-9944373091c2.jpg',
    brand: 'samsung',
    id: '870eca11-8ac4-f765-f9a5-9944373091c2',
    title: 'Tablet Samsung Galaxy Tab S2 32GB Tela 8" 4G Wi-Fi'
  },
  {
    price: 2349.0,
    image: 'http://challenge-api.luizalabs.com/images/bd0e779f-c224-2733-e671-5ad1df1d9d59.jpg',
    brand: 'samsung',
    id: 'bd0e779f-c224-2733-e671-5ad1df1d9d59',
    title: 'Tablet Samsung Galaxy Tab S2 32GB Tela 9,7" Wi-Fi'
  },
  {
    price: 2349.0,
    image: 'http://challenge-api.luizalabs.com/images/59d259ad-8d8b-70a2-95f7-60655205767b.jpg',
    brand: 'samsung',
    id: '59d259ad-8d8b-70a2-95f7-60655205767b',
    title: 'Tablet Samsung Galaxy Tab S2 32GB Tela 9,7" Wi-Fi'
  },
  {
    price: 2199.0,
    image: 'http://challenge-api.luizalabs.com/images/5fe02860-40ff-f414-ac49-127282ea357b.jpg',
    brand: 'tramontina',
    id: '5fe02860-40ff-f414-ac49-127282ea357b',
    title: 'Pia Fog\u00e3o 2 Bocas Tramontina 93723 El\u00e9trico Inox'
  },
  {
    price: 2199.0,
    image: 'http://challenge-api.luizalabs.com/images/226a5e4f-a22e-2422-3328-fd803b6260e1.jpg',
    brand: 'tramontina',
    id: '226a5e4f-a22e-2422-3328-fd803b6260e1',
    title: 'Pia Fog\u00e3o 2 Bocas Tramontina 93721 El\u00e9trico Inox'
  },
  {
    price: 2979.0,
    image: 'http://challenge-api.luizalabs.com/images/1c6d9f2a-db9b-3729-198a-1bfb2886baf6.jpg',
    brand: 'samsung',
    id: '1c6d9f2a-db9b-3729-198a-1bfb2886baf6',
    title: 'Tablet Samsung Galaxy Tab S2 32GB Tela 9,7" 4G'
  },
  {
    price: 119.9,
    image: 'http://challenge-api.luizalabs.com/images/77c72ee7-80fc-8df7-dbc5-d284e9574c7e.jpg',
    brand: 'logitech',
    id: '77c72ee7-80fc-8df7-dbc5-d284e9574c7e',
    title: 'Auto-falantes 12RMS 2.0 Canais Subwoofer Ativo'
  },
  {
    price: 89.9,
    image: 'http://challenge-api.luizalabs.com/images/eefc2589-ab13-99ad-a13e-da96c1878a8f.jpg',
    brand: 'publifolha',
    id: 'eefc2589-ab13-99ad-a13e-da96c1878a8f',
    title: 'Guia Visual Folha de S. Paulo'
  },
  {
    price: 79.9,
    image: 'http://challenge-api.luizalabs.com/images/406b690b-6370-cc02-ae3d-5f8cc30c5211.jpg',
    brand: 'logitech',
    id: '406b690b-6370-cc02-ae3d-5f8cc30c5211',
    title: 'Alto-falante 10W Subwoofer Ativo'
  },
  {
    price: 109.0,
    image: 'http://challenge-api.luizalabs.com/images/3fd41a38-2182-190b-9af0-0dd796d5e91d.jpg',
    brand: 'benetton',
    id: '3fd41a38-2182-190b-9af0-0dd796d5e91d',
    title: 'Benetton United Dreams Go Far Perfume Masculino'
  },
  {
    price: 79.0,
    image: 'http://challenge-api.luizalabs.com/images/44cd36ba-8a76-d824-0fb7-fbf62a412829.jpg',
    brand: 'benetton',
    id: '44cd36ba-8a76-d824-0fb7-fbf62a412829',
    title: 'Benetton United Dreams Go Far'
  },
  {
    price: 2059.9,
    image: 'http://challenge-api.luizalabs.com/images/74cfac01-6084-45af-d499-149f951c89f7.jpg',
    brand: 'tagima',
    id: '74cfac01-6084-45af-d499-149f951c89f7',
    title: 'Contrabaixo Tagima 5 Cordas'
  },
  {
    price: 79.0,
    image: 'http://challenge-api.luizalabs.com/images/005ed291-0d56-f557-2880-62599a889dd6.jpg',
    brand: 'benetton',
    id: '005ed291-0d56-f557-2880-62599a889dd6',
    title: 'Benetton United Dreams Be Strong Perfume'
  },
  {
    price: 79.0,
    image: 'http://challenge-api.luizalabs.com/images/a72722ae-6f90-c341-2517-278fa62c2e81.jpg',
    brand: 'benetton',
    id: 'a72722ae-6f90-c341-2517-278fa62c2e81',
    title: 'Benetton United Dreams Aim High Perfume'
  },
  {
    price: 3499.0,
    image: 'http://challenge-api.luizalabs.com/images/5d3f9d4b-fc58-bae3-f952-dc49c67bbdca.jpg',
    brand: 'samsung',
    id: '5d3f9d4b-fc58-bae3-f952-dc49c67bbdca',
    title: 'Ar-Condicionado Split Samsung 24000 BTUs'
  },
  {
    price: 7329.9,
    image: 'http://challenge-api.luizalabs.com/images/42ca7ead-f668-3eb5-76a9-9a4d683d2620.jpg',
    brand: 'roland',
    id: '42ca7ead-f668-3eb5-76a9-9a4d683d2620',
    title: 'Piano Digital'
  },
  {
    price: 1899.0,
    image: 'http://challenge-api.luizalabs.com/images/14370af7-51a0-f59a-82f1-c51c2bdbc914.jpg',
    brand: 'samsung',
    id: '14370af7-51a0-f59a-82f1-c51c2bdbc914',
    title: 'Ar-Condicionado Split Samsung 9000 BTUs Frio'
  },
  {
    price: 2579.9,
    image: 'http://challenge-api.luizalabs.com/images/77164b37-efd7-3349-ccf7-c81a55df4f0e.jpg',
    brand: 'staner',
    id: '77164b37-efd7-3349-ccf7-c81a55df4f0e',
    title: 'Combo Viol\u00e3o A 240'
  },
  {
    price: 1949.0,
    image: 'http://challenge-api.luizalabs.com/images/24b4abea-7610-2dd8-363f-6a2730d66680.jpg',
    brand: 'samsung',
    id: '24b4abea-7610-2dd8-363f-6a2730d66680',
    title: 'Ar-Condicionado Split Samsung 12000 BTUs'
  },
  {
    price: 2299.0,
    image: 'http://challenge-api.luizalabs.com/images/92592603-f72f-263a-ee6c-b2ab514837b1.jpg',
    brand: 'samsung',
    id: '92592603-f72f-263a-ee6c-b2ab514837b1',
    title: 'Ar-Condicionado Split Samsung 9000 BTUs'
  },
  {
    price: 3399.0,
    image: 'http://challenge-api.luizalabs.com/images/6b4afa96-0399-110d-6407-58330a05847f.jpg',
    brand: 'samsung',
    id: '6b4afa96-0399-110d-6407-58330a05847f',
    title: 'Ar-Condicionado Split Samsung 18000 BTUs'
  },
  {
    price: 497.5,
    image: 'http://challenge-api.luizalabs.com/images/df2bfe9f-2a30-639d-b9a5-5d7cfc6cfb11.jpg',
    brand: 'a criativa enxovais',
    id: 'df2bfe9f-2a30-639d-b9a5-5d7cfc6cfb11',
    title: 'Cortina para Home Office/Quarto/Sala Palha'
  },
  {
    price: 2699.0,
    image: 'http://challenge-api.luizalabs.com/images/ebe6d59b-3148-fa1c-ffd9-f7880852a2b3.jpg',
    brand: 'samsung',
    id: 'ebe6d59b-3148-fa1c-ffd9-f7880852a2b3',
    title: 'Ar-Condicionado Split Samsung 12000 BTUs'
  },
  {
    price: 497.5,
    image: 'http://challenge-api.luizalabs.com/images/3f0aca67-f5c3-6f17-618c-c9c7a76b0182.jpg',
    brand: 'a criativa enxovais',
    id: '3f0aca67-f5c3-6f17-618c-c9c7a76b0182',
    title: 'Cortina para Home Office/Quarto/Sala Ouro Velho'
  },
  {
    price: 3999.0,
    image: 'http://challenge-api.luizalabs.com/images/a85ecce3-27da-73cb-cb1e-bb7ce66c290a.jpg',
    brand: 'samsung',
    id: 'a85ecce3-27da-73cb-cb1e-bb7ce66c290a',
    title: 'Ar-Condicionado Split Samsung 24000 BTUs'
  },
  {
    price: 497.5,
    image: 'http://challenge-api.luizalabs.com/images/edd4019b-f83d-a033-059d-c9262c39c9cc.jpg',
    brand: 'a criativa enxovais',
    id: 'edd4019b-f83d-a033-059d-c9262c39c9cc',
    title: 'Cortina para Home Office/Quarto/Sala Branco'
  },
  {
    price: 39.9,
    image: 'http://challenge-api.luizalabs.com/images/18ffc15d-1e5a-7b33-1eca-d2e2ecc2e17a.jpg',
    brand: 'shirley may',
    id: '18ffc15d-1e5a-7b33-1eca-d2e2ecc2e17a',
    title: 'Shirley May Coffret Perfume Feminino'
  },
  {
    price: 469.0,
    image: 'http://challenge-api.luizalabs.com/images/ed00b363-8414-5a57-d341-85a08b11f03c.jpg',
    brand: 'valentino',
    id: 'ed00b363-8414-5a57-d341-85a08b11f03c',
    title: 'Valentino Kit Uomo'
  },
  {
    reviewScore: 3.2,
    title: 'Inalador e Nebulizador Ultrass\u00f4nico',
    price: 249.0,
    brand: 'soniclear',
    id: '243eca75-6e2e-1214-1a96-7d109e586b6f',
    image: 'http://challenge-api.luizalabs.com/images/243eca75-6e2e-1214-1a96-7d109e586b6f.jpg'
  },
  {
    price: 1248.9,
    image: 'http://challenge-api.luizalabs.com/images/c4af3907-72b1-8e98-5601-3bb0a8802d07.jpg',
    brand: 'sa\u00edda de emerg\u00eancia',
    id: 'c4af3907-72b1-8e98-5601-3bb0a8802d07',
    title: 'Smartphone Samsung Galaxy J5 Duos 16GB Dual Chip'
  },
  {
    reviewScore: 2.0,
    title: 'Freezer Vertical 1 Porta 173L',
    price: 1599.0,
    brand: 'electrolux',
    id: '4a8425c3-2f5f-0d58-92f0-43946a7e26a9',
    image: 'http://challenge-api.luizalabs.com/images/4a8425c3-2f5f-0d58-92f0-43946a7e26a9.jpg'
  },
  {
    price: 235.0,
    image: 'http://challenge-api.luizalabs.com/images/47678d1c-1c1d-2ee5-369f-47af81fb10d5.jpg',
    brand: 'issey miyake',
    id: '47678d1c-1c1d-2ee5-369f-47af81fb10d5',
    title: 'Issey Miyake LEau dIssey Florale'
  },
  {
    price: 289.0,
    image: 'http://challenge-api.luizalabs.com/images/4b667db4-2a9c-228a-0ce4-ed4fae054292.jpg',
    brand: 'lacoste',
    id: '4b667db4-2a9c-228a-0ce4-ed4fae054292',
    title: 'Lacoste Pour Homme'
  },
  {
    price: 69.9,
    image: 'http://challenge-api.luizalabs.com/images/5f97173f-a637-a6dc-2e44-c3688d3d94f6.jpg',
    brand: 'fibrasca',
    id: '5f97173f-a637-a6dc-2e44-c3688d3d94f6',
    title: 'Saia para Cama Queen Size Pliss\u00e9'
  },
  {
    price: 79.9,
    image: 'http://challenge-api.luizalabs.com/images/aa82f8ea-915a-1c11-f7b1-5decf1dc4fd4.jpg',
    brand: 'fibrasca',
    id: 'aa82f8ea-915a-1c11-f7b1-5decf1dc4fd4',
    title: 'Saia para Cama King Size Pliss\u00e9'
  },
  {
    price: 409.0,
    image: 'http://challenge-api.luizalabs.com/images/116bbbbb-9371-fa70-59df-196591cce650.jpg',
    brand: 'issey miyake',
    id: '116bbbbb-9371-fa70-59df-196591cce650',
    title: 'Issey Miyake LEau dIssey Florale'
  },
  {
    price: 139.0,
    image: 'http://challenge-api.luizalabs.com/images/d6cb67ed-e370-a56b-ff99-9b58e9e01e72.jpg',
    brand: 'shakira',
    id: 'd6cb67ed-e370-a56b-ff99-9b58e9e01e72',
    title: 'Shakira Live Perfume Feminino'
  },
  {
    price: 3099.0,
    image: 'http://challenge-api.luizalabs.com/images/111714b9-251e-8057-6b4f-0c52cbfd21da.jpg',
    brand: 'samsung',
    id: '111714b9-251e-8057-6b4f-0c52cbfd21da',
    title: 'Ar-Condicionado Split Samsung 18000 BTUs Frio'
  },
  {
    price: 69.0,
    image: 'http://challenge-api.luizalabs.com/images/a4f54646-cdb0-71ba-e349-c688d45e41c2.jpg',
    brand: 'shakira',
    id: 'a4f54646-cdb0-71ba-e349-c688d45e41c2',
    title: 'Shakira Love Rock! Perfume Feminino'
  },
  {
    price: 3299.0,
    image: 'http://challenge-api.luizalabs.com/images/cb477941-d3d5-7855-ea1f-434af352d632.jpg',
    brand: 'samsung',
    id: 'cb477941-d3d5-7855-ea1f-434af352d632',
    title: 'Ar-Condicionado Split Samsung 24000 BTUs Frio'
  },
  {
    price: 459.9,
    image: 'http://challenge-api.luizalabs.com/images/9b2d0800-5d77-11b9-6ef3-f581c84c59e7.jpg',
    brand: 'madrid',
    id: '9b2d0800-5d77-11b9-6ef3-f581c84c59e7',
    title: 'Viol\u00e3o Madrid MD 100 Ac\u00fastico Cl\u00e1ssico'
  },
  {
    price: 99.0,
    image: 'http://challenge-api.luizalabs.com/images/474bfb6d-d212-72fa-2216-6bcdfc1b48bb.jpg',
    brand: 'shakira',
    id: '474bfb6d-d212-72fa-2216-6bcdfc1b48bb',
    title: 'Shakira Love Rock! Perfume Feminino'
  },
  {
    reviewScore: 5.0,
    title: 'Ar-Condicionado Split Samsung 9000 BTUs Frio',
    price: 2099.0,
    brand: 'samsung',
    id: '863aaf32-4fd0-1d4e-3ee8-6ef6a668a59b',
    image: 'http://challenge-api.luizalabs.com/images/863aaf32-4fd0-1d4e-3ee8-6ef6a668a59b.jpg'
  },
  {
    price: 3290.0,
    image: 'http://challenge-api.luizalabs.com/images/fdd79555-67c0-a2b7-0b0c-b54a50ce9e48.jpg',
    brand: 'brastemp',
    id: 'fdd79555-67c0-a2b7-0b0c-b54a50ce9e48',
    title: 'Lavadora de Roupas Brastemp Ative! BWP11A9 11Kg'
  },
  {
    reviewScore: 5.0,
    title: 'Lavadora de Roupas Brastemp Ative! BWU11AE 11Kg',
    price: 2599.0,
    brand: 'brastemp',
    id: '1c797eab-1339-2c5e-00ac-951ab764c714',
    image: 'http://challenge-api.luizalabs.com/images/1c797eab-1339-2c5e-00ac-951ab764c714.jpg'
  },
  {
    price: 59.5,
    image: 'http://challenge-api.luizalabs.com/images/a6419296-acbb-6390-b225-2ef468a29fe2.jpg',
    brand: 'disal',
    id: 'a6419296-acbb-6390-b225-2ef468a29fe2',
    title: 'Graded Exercises In English'
  },
  {
    price: 245.0,
    image: 'http://challenge-api.luizalabs.com/images/6849afe6-166e-58e4-3dce-96f269aaef19.jpg',
    brand: 'issey miyake',
    id: '6849afe6-166e-58e4-3dce-96f269aaef19',
    title: 'Issey Miyake L\u00b4eau D\u00b4issey'
  },
  {
    price: 349.0,
    image: 'http://challenge-api.luizalabs.com/images/7621ca3c-34c6-61fb-783f-08cc638b4501.jpg',
    brand: 'lacoste',
    id: '7621ca3c-34c6-61fb-783f-08cc638b4501',
    title: 'Lacoste L.12.12 Bleu'
  },
  {
    reviewScore: 3.0,
    title: 'Adega Climatizada Electrolux 12 Garrafas ACS12',
    price: 879.0,
    brand: 'electrolux',
    id: 'b8765f44-0492-7ba4-de4f-1e09ff938f08',
    image: 'http://challenge-api.luizalabs.com/images/b8765f44-0492-7ba4-de4f-1e09ff938f08.jpg'
  },
  {
    price: 2790.0,
    image: 'http://challenge-api.luizalabs.com/images/854a1a5a-36f5-4b77-a185-fc4922afd3d6.jpg',
    brand: 'electrolux',
    id: '854a1a5a-36f5-4b77-a185-fc4922afd3d6',
    title: 'Geladeira/Refrigerador Electrolux Frost Free'
  },
  {
    price: 3129.0,
    image: 'http://challenge-api.luizalabs.com/images/0ae7c102-227a-aae0-393e-6cbed7b9aa19.jpg',
    brand: 'electrolux',
    id: '0ae7c102-227a-aae0-393e-6cbed7b9aa19',
    title: 'Geladeira/Refrigerador Electrolux Frost Free'
  },
  {
    price: 455.0,
    image: 'http://challenge-api.luizalabs.com/images/5572394b-226c-e60c-6197-df2c25e46418.jpg',
    brand: 'issey miyake',
    id: '5572394b-226c-e60c-6197-df2c25e46418',
    title: 'Issey Miyake L\u00b4eau D\u00b4issey Pour Homme'
  },
  {
    price: 599.0,
    image: 'http://challenge-api.luizalabs.com/images/37319d99-a7ef-8fa5-e788-76f5c45cc61e.jpg',
    brand: 'cadence',
    id: '37319d99-a7ef-8fa5-e788-76f5c45cc61e',
    title: 'Adega Climatizada Cadence 8 Garrafas Gourmet'
  },
  {
    price: 4090.0,
    image: 'http://challenge-api.luizalabs.com/images/97f2772b-11c0-70b0-243f-5ccc3a399120.jpg',
    brand: 'electrolux',
    id: '97f2772b-11c0-70b0-243f-5ccc3a399120',
    title: 'Geladeira/Refrigerador Electrolux Frost Free'
  }
]
