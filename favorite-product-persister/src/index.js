const { redisConnection } = require('@our-repo/databases')
const { add } = require('./client/add')
const { remove } = require('./client/remove')
const { errorHandler } = require('@our-repo/error-handler')

const redis = redisConnection()

redis.subscribe('addFavoriteProductReceived', 'removeFavoriteProductReceived', (err) => {
  if (err) throw err
  try {
    redis.on('messageBuffer', async (channel, message) => {
      const stringChannel = channel.toString()
      const { clientId, productId } = JSON.parse(message.toString())
      if (stringChannel === 'addFavoriteProductReceived') await add(clientId, productId)
      if (stringChannel === 'removeFavoriteProductReceived') await remove(clientId, productId)
    })
  } catch (e) {
    return errorHandler(e, null, { httpResponse: false })
  }
})
